#!/bin/bash

# Run this script from the alignment directory of one of the studies.

FILES=$(ls ../fastq/nicknames/*.fastq*)

for FASTQ in $FILES
do
  export FASTQ
  SAMPLE=${FASTQ%%.fastq*}
  SAMPLE=${SAMPLE#../fastq/nicknames/}
  export SAMPLE
  rm -r $SAMPLE #if there was an earlier version, get rid of it
  mkdir $SAMPLE
  qsub -e logs/$SAMPLE.log -o logs/$SAMPLE.log -v FASTQ,SAMPLE -N $SAMPLE runTophatBasic.pbs
done
