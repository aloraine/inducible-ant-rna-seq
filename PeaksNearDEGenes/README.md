# Peask / Motif Occurrences Near Genes

This module is designed to find relationships between the differential gene expression associated with DEX treatment (ie with ANT activation) and nearby genomic regions of interest.  These regions of interest could be iether motif occurrences or ChIP-seq peaks.  Because this module is designed for both motif occurences and peaks, both terms may appear.  

The TestAll_MA0571.1 Rmd is a direct followup for MA0571.1.Rmd.  The analysis is divided into to two major parts. The first part (MA0571.1.Rmd) handles data wrangling which includes merging tables and using other programs (ie bedtools) to process data.  This first part includes some visualizations to look at potential relationships, but never really asks a statistical question.  The second part (TestAll_MA0571.1.Rmd) is all about running statistical tests.  Both MA0571.1.Rmd and TestAll_MA0571.1.Rmd rely heavily on chunks.R.

Notice that many of code chunks in MA0571.1.Rmd are stored in chunks.R.  chunks.R has the bulk of the code needed for this analysis.  This ananlysis is likely to be repeated with slighly different inputs, such as ChIP-seq peaks or motif occurences based on a different motif in place of the MA0571 motif occurrences.  To accommodate this without having to duplicate large amount of code, the Rmd file is designed to be specific to one exact analysis (in this case, DE genes compared with MA0571.1 motif occurences found by FIMO), while the code that would be reused for the simliar analysis (DE genes compared with GCACA occurrences) is in chunks.R.  A new .Rmd file can added for the GCACA analysis, with file names and conclusions that are specific to that motif, while using the same general steps stored in chunks.R.  

For future edits:
If you choose to edit or add to any of the code in this module, please take time to understand and preserve this Rmd-and-chunks.R relationship.  The chunk reference method may feel slighly cumbersum at first because there is no way to reference chunks in interactive mode.  You have to flip between the two documents, or insert a save point so interactively view the environment produced by the markdown as it knits.



* * * 

## Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Beth Krizek krizek@sc.edu
* Ivory Blakley ieclabau@uncc.edu

* * *

## License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT