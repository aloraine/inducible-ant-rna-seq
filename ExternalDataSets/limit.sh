#!/bin/bash

# bedtools complained 
# Error: line number 13358 of file ../ExternalDatasets/Arabiopsis/TAIR10.bed.gz has 15 fields, but 14 were expected.

gunzip -c TAIR10.bed.gz | cut -f 1-12 > results/TAIR10_limited.bed 