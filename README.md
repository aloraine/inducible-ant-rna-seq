# RNA-Seq detects ANT targets

ANT is a DNA-binding protein in Arabidopsis that regulates aspects of floral development.
This experiment aims to answer the question:

* Which genes and pathways are directly regulated by ANT? 

* * *

## Experimental design

ANT-GR encodes ANT protein fused to the DNA-binding domain of the glucocorticoid receptor (GR).
In the absence of GR ligand, the GR domain blocks ANT-GR from entering the nucleus.
Dexamethasone (DEX) application allows ANT-GR to enter the nucleus and bindg to its targets.

Arabidopsis thaliana (Ler ecotype) plants transformed with 35S:ANT-GR were treated with DEX or a mock
DEX control. Samples consisting of stages 5 through 7 floral buds were collected 
2, 4, and 8 hours following treatment. 

In each treatment, one flat of plants received 10 micro molar DEX and a second, matched flat
received the mock treatment. Samples consisted of inflorescences with all unopened floral buds.

Samples were collected from four treatments performed on different days.
2 treatments x 4 replicates x 3 time points = 24 samples

At the time of treatment, plants were 27-28 days old, grown at 20 degrees C in 16 hour light/8 hour dark. 

Collections times:

 * Rep 1 8/28/14
 * Rep 2 8/29/14
 * Rep 3 8/30/14
 * Rep 4 9/2/14

Sample ids: 

 * Replicate number - 1 through 4
 * Treatment - dex or mock
 * Time (hours) between treatment and collection - 2, 4, 8

## About this project 

This project is a collaboration between the Krizek lab at University
of South Carolina and the Loraine Lab at UNC Charlotte.

A grant from the US National Science Foundation to PI Krizek and Co-PI
Ann Loraine funded this work.

* * *

## About this repository

This repository contains analysis folders named for the type of data
analysis code and results they contain.

Most analysis folders contain:

* .Rproj file - an RStudio project file. Open this in RStudio to re-run project code.
* .Rmd files - R Markdown files that contain an analysis focused on answering one or two discrete questions. These are typically formatted like miniature research articles, with Introduction, Results/Analysis, and Conclusions sections. 
* .html files - output from running knitr on .Rmd files. 
* results folders - contain files (typically tab-delimited) and images produced by .Rmd files. 
* data - data files obtained from a compute cluster or other external source upstream of this data analysis
* src - R, python, or perl scripts used for simple data processing tasks
* README.md - documentation 

Code used for analysis, making figures, and making data files for
visualization in external sites and programs are grouped into folders
with names indicating their common purpose or analysis type. 

Each module folder is designed to be run as a (mostly)
self-contained project in RStudio. As such, each folder contains an
".Rproj" file. To run the code in RStudio, just open the .Rproj file
and go from there. However, note that the code expects Unix-style file
paths and depends on external libraries, mainly from Bioconductor. 

Some modules depend on the output of other modules. Some modules also
depend on externally supplied data files, which are version-controlled
in ExternalDataSets but may also be available from external sites.

* * * 

## Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Beth Krizek krizek@sc.edu
* Ivory Blakley ieclabau@uncc.edu
* Nowlan Freese nfreese@uncc.edu

* * *

## License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT